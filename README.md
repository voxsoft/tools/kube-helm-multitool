# Kubernetes Helm Multi-Tool 
Source: https://gitlab.com/voxsoft/tools/kube-helm-multitool  
[![build passing](https://gitlab.com/voxsoft/tools/kube-helm-multitool/badges/master/pipeline.svg)](https://gitlab.com/voxsoft/tools/kube-helm-multitool/-/pipelines)
[![dockerhub-voxsoft](https://img.shields.io/docker/pulls/voxsoft/kube-helm-multitool.svg)](https://hub.docker.com/r/voxsoft/kube-helm-multitool)

Registries:
- [DockerHub](https://hub.docker.com/r/voxsoft/kube-helm-multitool): `docker pull voxsoft/kube-helm-multitool`
- [GitLab](https://gitlab.com/voxsoft/tools/kube-helm-multitool/container_registry): `docker pull registry.gitlab.com/voxsoft/tools/kube-helm-multitool`

## About this project
Just another container image that contains all required for typical kubernetes/helm build and deployment.  
The idea is to bring all tools required by typical kubernetes build and deploy process into one container image that
will be updated automatically if a new version of kubectl or helm is released

### Main Image
The main image contain:
- kubectl
- helm
- bash
- curl
- envsubst
- git
- jq


### Extra Tools
In addition to main image, also additional images are being built each time new kubectl/helm releases come.
- aws-cli
- aws-cli and docker-cli
- docker-cli
- gcloud-cli
- argocd-cli


### Image Tags Schema
Tag forming example

| Tag                       | Kubectl             | Helm              |  Extra tools | 
|:--------------------------|:-------------------:|:-----------------:|:---------:|
|v1.26.3-v3.11.2            |v1.26.3              |v3.11.2            | none |
|v1.26-v3.11                |v1.26 (latest patch) |v3.11 (latest patch)| none |
|v1.26-v3                   |v1.26 (latest patch) |v3 (latest minor)  | none |
|v1.26.6-v3.11.2-aws-docker |v1.26.3              |v3.11.2            | `aws-cli` and `docker-cli` |
|v1.26-v3.11-gcloud         |v1.26 (latest patch) |v3.11 (latest patch)| `gcloud-cli` |
|aws                        |(latest)             |(latest)           | `aws-cli` |
|aws-docker                 |(latest)             |(latest)           | `aws-cli` and `docker-cli` |
|gcloud                     |(latest)             |(latest)           | `gcloud-cli` |
|argocd                     |(latest)             |(latest)           | `argocd-cli` |

### Changelog:
[./CHANGELOG](./CHANGELOG)

# Contributing
Feel free to open an [issue](https://gitlab.com/voxsoft/tools/kube-helm-multitool/-/issues) 
or create [merge request](https://gitlab.com/voxsoft/tools/kube-helm-multitool/-/merge_requests) to improve this project.
