# docker buildx create --use default --name buildx-this
variable "MAIN_VERSIONS_RAW" {
  default = "v1.31.1-v3.16.1 v1.30.5-v3.15.4"
}
variable "MAIN_VERSIONS" {
  default = [ for item in split(" ", MAIN_VERSIONS_RAW) : { K8S_VERSION = split("-", item)[0], HELM_VERSION = split("-", item)[1] } ]
}

variable "BUILD_EXTRAS" {
  default = [
    "argocd",
    "aws-docker",
    "aws",
    "docker",
    "gcloud",
  ]
}
variable "CLOUD_SDK_VERSION" {
  default = "495.0.0"
}
variable "CONTAINER_REPOSITORIES" {
  default = [
    "docker.io/voxsoft/kube-helm-multitool",
    "registry.gitlab.com/voxsoft/tools/kube-helm-multitool"
  ]
}
variable "TEST_TAG_SUFFIX" {
  default = "-test"
}

function "minor" {
  params = [string]
  result = regex_replace(string, "\\.[0-9]+$", "")
}

group "default" {
  targets = [
    "main",
    "additional"
  ]
}

target "main" {
  matrix = {
    dockerfile_name = ["_parent"]
    versions        = MAIN_VERSIONS
  }
  name       = "${dockerfile_name}-${replace(versions.K8S_VERSION, ".", "-")}-${replace(versions.HELM_VERSION, ".", "-")}"
  dockerfile = "./dockerfiles/${dockerfile_name}.Dockerfile"
  tags = concat(
    [for repo in CONTAINER_REPOSITORIES : "${repo}:latest${TEST_TAG_SUFFIX}"],
    [for repo in CONTAINER_REPOSITORIES : "${repo}:${versions.K8S_VERSION}-${versions.HELM_VERSION}${TEST_TAG_SUFFIX}"],
    [for repo in CONTAINER_REPOSITORIES : "${repo}:${minor(versions.K8S_VERSION)}-${minor(versions.HELM_VERSION)}${TEST_TAG_SUFFIX}"],
    [for repo in CONTAINER_REPOSITORIES : "${repo}:${minor(versions.K8S_VERSION)}-v3${TEST_TAG_SUFFIX}"]
  )
  args = {
    K8S_VERSION  = versions.K8S_VERSION
    HELM_VERSION = versions.HELM_VERSION
  }
  output = ["type=image"]
}

target "additional" {
  matrix = {
    dockerfile_name = BUILD_EXTRAS
    versions        = MAIN_VERSIONS
  }
  name       = "${dockerfile_name}-${replace(versions.K8S_VERSION, ".", "-")}-${replace(versions.HELM_VERSION, ".", "-")}"
  contexts   = { parent = "target:_parent-${replace(versions.K8S_VERSION, ".", "-")}-${replace(versions.HELM_VERSION, ".", "-")}" }
  dockerfile = "./dockerfiles/${dockerfile_name}.Dockerfile"
  tags = concat(
    [for repo in CONTAINER_REPOSITORIES : "${repo}:${dockerfile_name}${TEST_TAG_SUFFIX}"],
    [for repo in CONTAINER_REPOSITORIES : "${repo}:${versions.K8S_VERSION}-${versions.HELM_VERSION}-${dockerfile_name}${TEST_TAG_SUFFIX}"],
    [for repo in CONTAINER_REPOSITORIES : "${repo}:${minor(versions.K8S_VERSION)}-${minor(versions.HELM_VERSION)}-${dockerfile_name}${TEST_TAG_SUFFIX}"],
    [for repo in CONTAINER_REPOSITORIES : "${repo}:${minor(versions.K8S_VERSION)}-v3-${dockerfile_name}${TEST_TAG_SUFFIX}"]
  )
  args = {
    CLOUD_SDK_VERSION = CLOUD_SDK_VERSION
  }
  output = ["type=image"]
}
