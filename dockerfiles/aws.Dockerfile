FROM parent
RUN apk add --no-cache aws-cli && \
    aws --version

CMD ["aws"]
