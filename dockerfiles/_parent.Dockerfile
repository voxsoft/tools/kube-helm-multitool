FROM alpine:3.20

LABEL maintainer="Oleksii Marchenko <oleksi.marchenko@gmail.com>"

ARG K8S_VERSION
ARG HELM_VERSION

RUN apk add --no-cache --update \
        bash \
        ca-certificates \
        curl \
        git \
        gettext \
        jq \
        tar \
        gzip && \
    curl -L https://dl.k8s.io/release/${K8S_VERSION}/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl && \
    curl -L https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz | tar xz && mv linux-amd64/helm /bin/helm && rm -rf linux-amd64 && \
    chmod +x /usr/local/bin/kubectl && \
    kubectl version --client && \
    helm version --client

CMD ["helm"]
