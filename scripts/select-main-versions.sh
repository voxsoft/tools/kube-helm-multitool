#!/usr/bin/env bash
set -e
K8S_MAJOR_VERSION=v1
HELM_MAJOR_VERSION=v3
K8S_VERSIONS=$(curl -Ls https://github.com/kubernetes/kubernetes/tags | grep -oE "$K8S_MAJOR_VERSION\.[0-9]+\.[0-9]+\.tar\.gz" | cut -d '.' -f -3 | sort -r | uniq )
HELM_VERSIONS=$(curl -Ls https://github.com/helm/helm/tags           | grep -oE "$HELM_MAJOR_VERSION\.[0-9]+\.[0-9]+\.tar\.gz" | cut -d '.' -f -3 | sort -r | uniq )

function ver_pick_latest_patches() {
  local minors=$(echo $2 | grep -oE "$1\.[0-9]+\." | cut -d '.' -f -2 | sort -r | uniq | head -n $3)
  for minor in $minors; do
    local patch=$(echo $2 | grep -oE "$minor\.[0-9]+" | sort -r | head -n 1 )
    local picked+=($patch)
  done
  echo "${picked[@]}"
}

K8S_VERSIONS_LATEST_PATCHES=$(ver_pick_latest_patches "$K8S_MAJOR_VERSION" "$K8S_VERSIONS" "3")
HELM_VERSIONS_LATEST_PATCHES=$(ver_pick_latest_patches "$HELM_MAJOR_VERSION" "$HELM_VERSIONS" "1")

for k8s_version in $K8S_VERSIONS_LATEST_PATCHES; do
  for helm_version in $HELM_VERSIONS_LATEST_PATCHES; do
    MAIN_VERSION_BUNDLES_SUGGESTED+=("$k8s_version-$helm_version")
  done
done

echo -e "
Found versions:
  k8s: $(echo $K8S_VERSIONS)
  helm: $(echo $HELM_VERSIONS)

Latest patches:
  k8s: $(echo $K8S_VERSIONS_LATEST_PATCHES)
  helm: $(echo $HELM_VERSIONS_LATEST_PATCHES)

Suggested version bundles:
$(printf "  %s\n" "${MAIN_VERSION_BUNDLES_SUGGESTED[@]}")

Checking if version bundles are already published...
"

for version_bundle in ${MAIN_VERSION_BUNDLES_SUGGESTED[@]}; do
  docker manifest inspect voxsoft/kube-helm-multitool:${version_bundle} > /dev/null && \
    echo -e "\e[44mVersion bundle ${version_bundle} is already published. Skipping\e[0m" || \
  MAIN_VERSIONS_FILTERED+=($version_bundle)
done

echo -e "
Start to build version bundles:
$(printf "  %s\n" "${MAIN_VERSIONS_FILTERED[@]}")

"

if [ "$CI" != "true" ]; then
  if [ -z "$FORCE_BUILD_VERSIONS_BUNDLE" ]; then
    MAIN_VERSIONS_RAW="${MAIN_VERSIONS_FILTERED[@]}"
  else
    MAIN_VERSIONS_RAW="${FORCE_BUILD_VERSIONS_BUNDLE}"
  fi

  if [ -z "$MAIN_VERSIONS_RAW" ]; then
    echo -e "\e[42mNothing to build. Exiting\e[0m"
    exit 0
  else
    MAIN_VERSIONS_RAW=$MAIN_VERSIONS_RAW docker buildx bake ## [--print|--push]
  fi
fi
